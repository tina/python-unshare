Source: python-unshare
Maintainer: Martín Ferrari <tincho@debian.org>
Section: python
Priority: optional
Build-Depends: python-all-dev, debhelper (>= 9), dh-python
Standards-Version: 3.9.8
Homepage: https://github.com/TheTincho/python-unshare/
Vcs-Browser: https://anonscm.debian.org/cgit/users/tincho/python-unshare.git
Vcs-Git: https://anonscm.debian.org/git/users/tincho/python-unshare.git

Package: python-unshare
Architecture: linux-any
Depends: ${misc:Depends}, ${python:Depends}, ${shlibs:Depends}
XB-Python-Version: ${python:Versions}
Provides: ${python:Provides}
Description: Python bindings for the Linux unshare() syscall
 This simple extension provides bindings to the Linux unshare() syscall, added
 in kernel version 2.6.16.
 .
 By using unshare(), new and interesting features of the Linux kernel can be
 exploited, such as:
 .
  * Creating a new network name space (CLONE_NEWNET)
  * Creating a new file system mount name space (CLONE_NEWNS)
  * Reverting other features shared from clone()
 .
 This library provides an equivalent of the util-linux command-line program
 unshare. 
